/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLocale>
#include <QDebug>
#include <QClipboard>
#include <QMenu>
#include <QTimer>
#include <QMimeData>
#include <QSettings>
#include <QThread>

#if defined(Q_OS_WIN)
#include <QMimeData>
#include <QTimer>
#include <windows.h>
#endif

#include "settingsdialog.h"
#include "windowform.h"
#include "wordswitcher.h"

int MainWindow::firstPreferedLanguage;
int MainWindow::secondPreferedLanguage;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    trayMenu (new QMenu(this)),
    trayIcon (new QSystemTrayIcon(this)),
    showPopupHotkey (new QHotkey(this)),
    switchSelectionHotkey (new QHotkey(this)),
    settingsHotkey(new QHotkey(this)),
    exitShortcut(new QShortcut(this)),
    convertShortcut(new QShortcut(this)),
    copyConvertedHotkey(new QShortcut(this))
{
    // Localization
    QSettings settings;
    QString localeCode = settings.value("Language", "auto").toString();
    if (localeCode != "auto")
        QLocale::setDefault(QLocale(localeCode));
    if (translator.load(QLocale(), QString("qweKadabra"), QString("_"), QString(":/translations")))
        qApp->installTranslator(&translator);

    ui->setupUi(this);
    ui->inputComboBox->setItemData(0, WordSwitcher::English);
    ui->inputComboBox->setItemData(1, WordSwitcher::Russian);
    ui->inputComboBox->setItemData(2, WordSwitcher::Ukrainian);
    
    ui->outputComboBox->setItemData(0, WordSwitcher::English);
    ui->outputComboBox->setItemData(1, WordSwitcher::Russian);
    ui->outputComboBox->setItemData(2, WordSwitcher::Ukrainian);

    ui->outputComboBox->setCurrentIndex(1);

    // Settings
    firstPreferedLanguage = settings.value("FirstPreferredLanguage", WordSwitcher::English).toInt();
    secondPreferedLanguage = settings.value("SecondPreferredLanguage", WordSwitcher::Russian).toInt();

    // Tray Icon
    trayMenu->addAction(QIcon::fromTheme("window"), tr("Show window"), this, &MainWindow::show);
    trayMenu->addAction(QIcon::fromTheme("application-exit"), tr("Exit"), qApp, &QApplication::quit);
    trayIcon->setContextMenu(trayMenu);
    trayIcon->setIcon(QIcon("://data/qweKadabra-tray.svg"));
    connect(trayIcon, &QSystemTrayIcon::activated, [&](QSystemTrayIcon::ActivationReason reason) {
        if (reason == QSystemTrayIcon::Trigger) {
            if (!this->isVisible()) {
                showNormal();
                activateWindow();
            }
        else
                hide();
        }
    });
    trayIcon->setVisible(true);
    QApplication::setQuitOnLastWindowClosed(false);

    // Hotkeys and Shortcuts connects with slots // Горячие клавиши соединяются с действиями
    connect(showPopupHotkey, &QHotkey::activated, this, &MainWindow::showPopup);
    connect(switchSelectionHotkey, &QHotkey::activated, this, &MainWindow::switchSelection);
    connect(copyConvertedHotkey, &QShortcut::activated, this, &MainWindow::on_copyToButton_clicked);
    connect(settingsHotkey, &QHotkey::activated, this, &MainWindow::on_settingsButton_clicked);
    connect(convertShortcut, &QShortcut::activated, this, &MainWindow::on_swichButton_clicked);
    connect(exitShortcut, &QShortcut::activated, qApp, &QApplication::quit);

    // Hotkeys set value // Получение значений горячих клавиш из настроек
    showPopupHotkey->setShortcut(QKeySequence(settings.value("Shortcuts/ConvertGlobalKeySequence", "Ctrl+Shift+S").toString()), true);
    switchSelectionHotkey->setShortcut(QKeySequence(settings.value("Shortcuts/ConvertAndReplaceKeySequence", "Ctrl+Shift+D").toString()), true);
    settingsHotkey->setShortcut(QKeySequence(settings.value("Shortcuts/SettingsKeySequence", "Ctrl+Shift+A").toString()), true);
    exitShortcut->setKey(QKeySequence(settings.value("Shortcuts/ExitKeySequence", "Ctrl+Q").toString()));
    convertShortcut->setKey(QKeySequence(settings.value("Shortcuts/ConvertMWKeySequence", "Ctrl+Enter").toString()));
    copyConvertedHotkey->setKey(QKeySequence(settings.value("Shortcuts/CopyKeySequence", "Ctrl+Shift+C").toString()));

    loadSettings();
}

QString MainWindow::copySelectedText()
{
    QString selectedText;
#if defined(Q_OS_LINUX)
    selectedText = QApplication::clipboard()->text(QClipboard::Selection);
#elif defined(Q_OS_WIN) // Send Ctrl + C to get selected text

    // Generate key sequence
    INPUT copyText[4];

    // Set the press of the "Ctrl" key
    copyText[0].ki.wVk = VK_CONTROL;
    copyText[0].ki.dwFlags = 0; // 0 for key press
    copyText[0].type = INPUT_KEYBOARD;

    // Set the press of the "C" key
    copyText[1].ki.wVk = 'C';
    copyText[1].ki.dwFlags = 0;
    copyText[1].type = INPUT_KEYBOARD;

    // Set the release of the "C" key
    copyText[2].ki.wVk = 'C';
    copyText[2].ki.dwFlags = KEYEVENTF_KEYUP;
    copyText[2].type = INPUT_KEYBOARD;

    // Set the release of the "Ctrl" key
    copyText[3].ki.wVk = VK_CONTROL;
    copyText[3].ki.dwFlags = KEYEVENTF_KEYUP;
    copyText[3].type = INPUT_KEYBOARD;

    // Send key sequence to system
    SendInput(4, copyText, sizeof(INPUT));

    // Wait for the clipboard to change
    QEventLoop loop;
    QTimer timer; // Add a timer for the case where the text is not selected
    loop.connect(QApplication::clipboard(), &QClipboard::changed, &loop, &QEventLoop::quit);
    loop.connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    timer.start(1000);
    loop.exec();

    // Workaround for the case where the clipboard has changed but not yet available
    if (QGuiApplication::clipboard()->text().isEmpty())
        QThread::msleep(1);

    // Get clipboard data
    selectedText = QApplication::clipboard()->text();

#endif
    return selectedText;
}

#if defined(Q_OS_WIN) // Send Ctrl + V to send text from clipboard
void MainWindow::pasteText(const QString &text)
{
    // Send text to clipboard
    QGuiApplication::clipboard()->setText(text);

    // Generate key sequence
    INPUT copyText[4];

    // Set the press of the "Ctrl" key
    copyText[0].ki.wVk = VK_CONTROL;
    copyText[0].ki.dwFlags = 0; // 0 for key press
    copyText[0].type = INPUT_KEYBOARD;

    // Set the press of the "V" key
    copyText[1].ki.wVk = 'V';
    copyText[1].ki.dwFlags = 0;
    copyText[1].type = INPUT_KEYBOARD;

    // Set the release of the "V" key
    copyText[2].ki.wVk = 'V';
    copyText[2].ki.dwFlags = KEYEVENTF_KEYUP;
    copyText[2].type = INPUT_KEYBOARD;

    // Set the release of the "Ctrl" key
    copyText[3].ki.wVk = VK_CONTROL;
    copyText[3].ki.dwFlags = KEYEVENTF_KEYUP;
    copyText[3].type = INPUT_KEYBOARD;

    // Send key sequence to system
    SendInput(4, copyText, sizeof(INPUT));
}
#endif

void MainWindow::on_swichButton_clicked()
{
    switcher.switchLayout(ui->inputTextEdit->toPlainText(),
                          static_cast<WordSwitcher::Layout>(ui->outputComboBox->currentData().toInt()),
                          static_cast<WordSwitcher::Layout>(ui->inputComboBox->currentData().toInt()));
    ui->outputTextEdit->setPlainText(switcher.getReplacedText());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showPopup()
{
    showPopupHotkey->blockSignals(true);
#if defined(Q_OS_WIN)
    saveClipboard();
    waitForKeyUnpressed(showPopupHotkey);
#endif

    QString selectedText = copySelectedText();

#if defined(Q_OS_WIN)
    restoreClipboard();
#endif

// Set language for traslation // Установить язык на который будет осуществляться перевод
    if (switcher.getLayoutName() == firstPreferedLanguage)
        switcher.switchLayout(selectedText,
                              static_cast<WordSwitcher::Layout>(secondPreferedLanguage));
//        switcher.switchLayout(selectedText,
//                          static_cast<WordSwitcher::Layout>(ui->outputComboBox->currentData().toInt()));
    else if (switcher.getLayoutName() == secondPreferedLanguage)
        switcher.switchLayout(selectedText,
                              static_cast<WordSwitcher::Layout>(firstPreferedLanguage));
    else
        switcher.switchLayout(selectedText,
                               static_cast<WordSwitcher::Layout>(firstPreferedLanguage));

    windowForm *wind = new windowForm(switcher.getLayoutName(), this);
    connect(wind, &windowForm::destroyed, [&] {
        showPopupHotkey->blockSignals(false);
    });

    wind->setData(switcher.getReplacedText());
    wind->setSelectedText(selectedText);
    wind->show();
}

void MainWindow::switchSelection()
{
#if defined(Q_OS_LINUX)
    qDebug() << "Not Implemented!";
#elif defined(Q_OS_WIN)
    waitForKeyUnpressed(switchSelectionHotkey);

    saveClipboard();
    QString selectedText = copySelectedText();

    // Set language for traslation // Установить язык на который будет осуществляться перевод
    if (switcher.getLayoutName() == firstPreferedLanguage)
        switcher.switchLayout(selectedText,
                              static_cast<WordSwitcher::Layout>(secondPreferedLanguage));
    else if (switcher.getLayoutName() == secondPreferedLanguage)
        switcher.switchLayout(selectedText,
                              static_cast<WordSwitcher::Layout>(firstPreferedLanguage));
    else
        switcher.switchLayout(selectedText,
                              static_cast<WordSwitcher::Layout>(firstPreferedLanguage));

    pasteText(switcher.getReplacedText());

    // Wait for the paste //Подождать перед вставкой
    QEventLoop loop;
    QTimer timer;
    loop.connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    timer.start(1000);
    loop.exec();
    restoreClipboard();
#endif
}

#if defined(Q_OS_WIN)
void MainWindow::waitForKeyUnpressed(QHotkey *hotkey)
{
    while (GetAsyncKeyState(static_cast<int>(hotkey->currentNativeShortcut().key)) ||
           GetAsyncKeyState(VK_CONTROL) ||
           GetAsyncKeyState(VK_MENU) ||
           GetAsyncKeyState(VK_SHIFT)) {
        Sleep(50);
    }
}
#endif

// Save original clipboard data  // Сохранить данные буфера обмена
void MainWindow::saveClipboard()
{
    if (QApplication::clipboard()->mimeData()->hasImage())
        originalClipboard = QApplication::clipboard()->image();
    else
        originalClipboard = QApplication::clipboard()->text();
}

// Return original clipboard // Вернуть данные буфера  обмена
void MainWindow::restoreClipboard()
{
    if (originalClipboard.type() == QVariant::Image)
        QApplication::clipboard()->setImage(originalClipboard.value<QImage>());
    else
        QApplication::clipboard()->setText(originalClipboard.toString());
}

void MainWindow::on_inputComboBox_currentIndexChanged(int index)
{
    if (index == ui->outputComboBox->currentIndex()) {
        ui->outputComboBox->setCurrentIndex(inputPreviousID);
    }
    inputPreviousID = index;
}

void MainWindow::on_outputComboBox_currentIndexChanged(int index)
{
    if (index == ui->inputComboBox->currentIndex()) {
        ui->inputComboBox->setCurrentIndex(outputPreviousID);
    }
    outputPreviousID = index;
}

void MainWindow::on_swapLayoutButton_clicked()
{
    ui->outputComboBox->setCurrentIndex(ui->inputComboBox->currentIndex());
}

void MainWindow::on_automaticReverseBox_toggled(bool checked)
{
    if (checked == true) {
        connect(ui->inputTextEdit, &QPlainTextEdit::textChanged, this, &MainWindow::on_swichButton_clicked);
    }
    else {
        disconnect(ui->inputTextEdit, &QPlainTextEdit::textChanged, this, &MainWindow::on_swichButton_clicked);
    }
}

void MainWindow::on_copyFromButton_clicked()
{
    if (ui->inputTextEdit->toPlainText() != "") {
        ui->inputTextEdit->selectAll();
        ui->inputTextEdit->copy();
    }
    else
        qDebug() << tr("Text field is empty");
}

void MainWindow::on_copyToButton_clicked()
{
    if (ui->outputTextEdit->toPlainText() != "") {
        ui->outputTextEdit->selectAll();
        ui->outputTextEdit->copy();
    }
    else
        qDebug() << tr("Text field is empty");
}

void MainWindow::on_settingsButton_clicked()
{
    SettingsDialog setting;
    if(setting.exec()) {
//        qDebug() << setting.localizationChanged();
        if (setting.localizationChanged() == true) {
//            qDebug() << setting.localizationChanged();
            // Reload localization // Перегрузить локализацию
            QSettings settings;
            QString localeCode = settings.value("LocaleLanguage", "auto").toString();
            if (localeCode == "auto") {
                QLocale::setDefault(QLocale::system());
            }
            else
                QLocale::setDefault(QLocale(localeCode));
            if (translator.load(QLocale(), QString("qweKadabra"), QString("_"), QString(":/translations")))
                qApp->installTranslator(&translator);

            // Reload UI // Перегрузить пользовательский интерфейс
            ui->retranslateUi(this);
            trayMenu->actions().at(0)->setText(tr("Show window"));
            trayMenu->actions().at(1)->setText(tr("Exit"));
        }

        setting.done(0);
        loadSettings();
    }
}

void MainWindow::loadSettings()
{
     QSettings settings;

     // System tray icon
     QString iconName = settings.value("TrayIcon", ":/data/qweKadabra-tray.svg").toString();
     if (iconName == "custom") {
         QIcon customIcon(settings.value("CustomIconPath", "").toString());
         if (customIcon.isNull())
             trayIcon->setIcon(QIcon::fromTheme("dialog-error"));
         else
             trayIcon->setIcon(customIcon);
     } else {
         QIcon qweKadabraIcon(iconName);
         if (qweKadabraIcon.isNull())
             trayIcon->setIcon(QIcon::fromTheme("dialog-error"));
         else
             trayIcon->setIcon(qweKadabraIcon);
     }

     // Hotkeys set value // Получение значений горячих клавиш из настроек
     showPopupHotkey->setShortcut(QKeySequence(settings.value("Shortcuts/ConvertGlobalKeySequence", "Ctrl+Shift+S").toString()), true);
     switchSelectionHotkey->setShortcut(QKeySequence(settings.value("Shortcuts/ConvertAndReplaceKeySequence", "Ctrl+Shift+D").toString()), true);
     settingsHotkey->setShortcut(QKeySequence(settings.value("Shortcuts/SettingsKeySequence", "Ctrl+Shift+A").toString()), true);
     exitShortcut->setKey(QKeySequence(settings.value("Shortcuts/ExitKeySequence", "Ctrl+Q").toString()));
     convertShortcut->setKey(QKeySequence(settings.value("Shortcuts/ConvertMWKeySequence", "Ctrl+Enter").toString()));
     copyConvertedHotkey->setKey(QKeySequence(settings.value("Shortcuts/CopyKeySequence", "Ctrl+Shift+C").toString()));
}
