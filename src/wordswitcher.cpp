/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "wordswitcher.h"

#include <QDebug>

#if defined(Q_OS_LINUX)
#include "XKeyboard.h"
#elif defined(Q_OS_WIN32)
#include <windows.h>
#endif

void WordSwitcher::switchLayout(const QString &data, Layout layoutTo, Layout layoutFrom)
{
    replacedText.clear();

    if (layoutFrom == Unknown)
        layoutFrom = getCurrentLayout();

    QString layoutStringTo = getLayoutString(layoutTo);
    QString layoutStringFrom = getLayoutString(layoutFrom);

    if (layoutStringFrom == "") {
        replacedText = tr("Unknown layout. You must select RU, ENG or UKR");
        return;
    }


    for (int i = 0; i < data.size(); ++i) {
        bool replaced = false;

        for (int j = 0; j < layoutStringFrom.size(); ++j) {
            if (data.at(i) == layoutStringFrom[j]) {
                replacedText += layoutStringTo[j];
                replaced = true;
                continue;
            }
        }

        if (!replaced)
            replacedText += data.at(i);
    }
}

QString WordSwitcher::getLayoutString(Layout layout)
{
    switch (layout) {
    case English:
        return ENGLISH_LAYOUT;
    case Russian:
        return RUSSIAN_LAYOUT;
    case Ukrainian:
        return UKRAINIAN_LAYOUT;
    default:
    {
        qDebug() << "Unknown layout ID";
        return "";
    }
    }
}

WordSwitcher::Layout WordSwitcher::getCurrentLayout()
{
#if defined(Q_OS_WIN32)
    HWND hwnd = GetForegroundWindow();
    if (hwnd) {
      DWORD threadID = GetWindowThreadProcessId(hwnd, nullptr);
      HKL currentLayout = GetKeyboardLayout(threadID);
      unsigned id = reinterpret_cast<unsigned int>(currentLayout) & 0x0000FFFF;
      return static_cast<Layout>(id);
    }
    return Unknown;
#elif defined(Q_OS_LINUX)
    XKeyboard xkb;
    std::string layoutName = xkb.currentGroupName();
    if (layoutName == "English")
        return English;
    else if (layoutName == "Russian")
        return Russian;
    else if (layoutName == "Ukrainian")
        return Ukrainian;
    else
        return Unknown;
#endif
}

QString WordSwitcher::getReplacedText() const
{
    return replacedText;
}

int WordSwitcher::getLayoutName()
{
    return getCurrentLayout();
    //    Layout layout = getCurrentLayout();
//    if (layout == 1033)
//        return "English";
//    else if (layout == 1049)
//        return "Russian";
//    else if (layout == 1058)
//        return "Ukrainian";
//    else
//        return "English";
}
