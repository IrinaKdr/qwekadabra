/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "windowform.h"
#include "mainwindow.h"

#include <QScreen>
#include <QDebug>
#include <QSettings>

#include "ui_windowform.h"

windowForm::windowForm(int layout, QWidget *parent) :
    QWidget(parent, Qt::Popup),
    ui(new Ui::windowForm),
    copyConvertedHotkey(new QShortcut(this))
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose); //Delete window after clouse // Удалить окно после закрытия

    //Settings // Подключение параметров настроек
    QSettings settings;
    setWindowOpacity(settings.value("Interface/Transparency", 1).toReal());
    resize(settings.value("Interface/PopupSize", QSize(265, 150)).toSize());
    ui->textBrowser->setStyleSheet("font: " + QString::number(settings.value("Interface/FontSize", 10).toInt()) + "pt;");
    copyConvertedHotkey->setKey(QKeySequence(settings.value("Shortcuts/CopyKeySequence", "Ctrl+Shift+C").toString()));
    connect(copyConvertedHotkey, &QShortcut::activated, this, &windowForm::on_copyButton_clicked);

    // Move to cursor //Поместить поле в точку нахождения курсора
    QPoint position = QCursor::pos(); // Cursor position
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    QSize availableSize = QGuiApplication::screenAt(position)->availableSize();
#else
    QSize availableSize = QApplication::desktop()->screenGeometry(position).size();
#endif

    if (availableSize.width() - position.x() - this->geometry().width() < 0) {
        position.rx()-= this->frameGeometry().width();
        if (position.x() < 0)
            position.rx() = 0;
    }
    if (availableSize.height() - position.y() - this->geometry().height() < 0) {
        position.ry()-= this->frameGeometry().height();
        if (position.y() < 0)
            position.ry() = 0;
    }

    move(position);

    ui->inputComboBox->setItemData(0, WordSwitcher::English);
    ui->inputComboBox->setItemData(1, WordSwitcher::Russian);
    ui->inputComboBox->setItemData(2, WordSwitcher::Ukrainian);

    ui->outputComboBox->setItemData(0, WordSwitcher::English);
    ui->outputComboBox->setItemData(1, WordSwitcher::Russian);
    ui->outputComboBox->setItemData(2, WordSwitcher::Ukrainian);


    ui->inputComboBox->setCurrentIndex(ui->inputComboBox->findData(layout));
    if (layout == MainWindow::firstPreferedLanguage)
        ui->outputComboBox->setCurrentIndex(ui->outputComboBox-> findData(MainWindow::secondPreferedLanguage));
    else if (layout == MainWindow::secondPreferedLanguage)
        ui->outputComboBox->setCurrentIndex(ui->outputComboBox-> findData(MainWindow::firstPreferedLanguage));
    else
        ui->outputComboBox->setCurrentIndex(ui->outputComboBox-> findData(MainWindow::secondPreferedLanguage));

    inputPreviousID = ui->inputComboBox->currentIndex();
    outputPreviousID = ui->outputComboBox->currentIndex();
}

void windowForm::setData(QString data)
{
    ui->textBrowser->setPlainText(data);
}

void windowForm::setSelectedText(QString text)
{
    selectedText = text;
}

windowForm::~windowForm()
{
    delete ui;
}

void windowForm::on_copyButton_clicked()
{
    if (ui->textBrowser->toPlainText() != "") {
        ui->textBrowser->selectAll();
        ui->textBrowser->copy();
    }
    else
        qDebug() << tr("Text field is empty");
}

void windowForm::on_swichButton_clicked()
{
    switcher.switchLayout(selectedText,
                          static_cast<WordSwitcher::Layout>(ui->outputComboBox->currentData().toInt()),
                          static_cast<WordSwitcher::Layout>(ui->inputComboBox->currentData().toInt()));
    ui->textBrowser->setPlainText(switcher.getReplacedText());
}

void windowForm::on_inputComboBox_currentIndexChanged(int index)
{
    if (index == ui->outputComboBox->currentIndex()) {
        ui->outputComboBox->setCurrentIndex(inputPreviousID);
    }
    inputPreviousID = index;
}


void windowForm::on_outputComboBox_currentIndexChanged(int index)
{
    if (index == ui->inputComboBox->currentIndex()) {
        ui->inputComboBox->setCurrentIndex(outputPreviousID);
    }
    outputPreviousID = index;
}
