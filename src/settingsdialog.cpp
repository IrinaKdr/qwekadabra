/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "windowform.h"

#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QStandardPaths>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    //Pages selection //Выбранные страницы
    connect(ui->listWidget, &QListWidget::currentRowChanged, ui->stackedWidget, &QStackedWidget::setCurrentIndex);

    ui->firstPreferredLanguage->setItemData(0, WordSwitcher::English);
    ui->firstPreferredLanguage->setItemData(1, WordSwitcher::Russian);
    ui->firstPreferredLanguage->setItemData(2, WordSwitcher::Ukrainian);

    ui->secondPreferredLanguage->setItemData(0, WordSwitcher::English);
    ui->secondPreferredLanguage->setItemData(1, WordSwitcher::Russian);
    ui->secondPreferredLanguage->setItemData(2, WordSwitcher::Ukrainian);

    ui->trayIconBox->setItemData(0, ":/data/qweKadabra-tray.svg");
    ui->trayIconBox->setItemData(1, ":/data/qweKadabra-icon-white.svg");
    ui->trayIconBox->setItemData(2, ":/data/qweKadabra.png");
    ui->trayIconBox->setItemData(3, ":/data/qweKadabra-icon-blue.svg");
    ui->trayIconBox->setItemData(4, ":/data/qweKadabra-icon-dark-blue.svg");
    ui->trayIconBox->setItemData(5, ":/data/qweKadabra-icon-green.svg");
    ui->trayIconBox->setItemData(6, ":/data/qweKadabra-icon-orange.svg");
    ui->trayIconBox->setItemData(7, ":/data/qweKadabra-icon-pink.svg");
    ui->trayIconBox->setItemData(8, ":/data/qweKadabra-icon-red.svg");
    ui->trayIconBox->setItemData(9, ":/data/qweKadabra-icon-yellow.svg");
    ui->trayIconBox->setItemData(10, "custom");

    ui->localeLanguage->setItemData(0, "auto");
    ui->localeLanguage->setItemData(1, "en");
    ui->localeLanguage->setItemData(2, "ru");

    ui->versionLabel->setText(qApp->applicationVersion());


    QSettings settings;

    //General //Главное
    ui->localeLanguage->setCurrentIndex(ui->localeLanguage->findData(settings.value("LocaleLanguage", "auto").toString()));
    ui->firstPreferredLanguage->setCurrentIndex(ui->firstPreferredLanguage->findData(settings.value("FirstPreferredLanguage", WordSwitcher::English).toInt()));
    ui->secondPreferredLanguage->setCurrentIndex(ui->secondPreferredLanguage->findData(settings.value("SecondPreferredLanguage", WordSwitcher::Russian).toInt()));
    ui->autostartCheckBox->setChecked(settings.value("AutostartCheckBox", false).toBool());
    ui->minimizedCheckBox->setChecked(settings.value("MinimizedCheckBox", false).toBool());
    ui->trayCheckBox->setChecked(settings.value("TrayCheckBox", true).toBool());

    //Соеденить слайдеры со спинбоксами
    connect(ui->transparencySlider, &QSlider::valueChanged, ui->transparencySpinBox, &QSpinBox::setValue);
    connect(ui->heightSlider, &QSlider::valueChanged, ui->heightSpinBox, &QSpinBox::setValue);
    connect(ui->widthSlider, &QSlider::valueChanged, ui->widthSpinBox, &QSpinBox::setValue);
    connect(ui->fontSizeSlider, &QSlider::valueChanged, ui->fontSizeSpinBox, &QSpinBox::setValue);
    //
    connect(ui->transparencySpinBox, qOverload<int>(&QSpinBox::valueChanged), ui->transparencySlider, &QSlider::setValue);
    connect(ui->heightSpinBox, qOverload<int>(&QSpinBox::valueChanged), ui->heightSlider, &QSlider::setValue);
    connect(ui->widthSpinBox, qOverload<int>(&QSpinBox::valueChanged), ui->widthSlider, &QSlider::setValue);
    connect(ui->fontSizeSpinBox, qOverload<int>(&QSpinBox::valueChanged), ui->fontSizeSlider, &QSlider::setValue);

    //Interface //Интерфейс
    ui->transparencySlider->setValue(static_cast<int>(settings.value("Interface/Transparency", 1).toReal() * 100));
    ui->heightSlider->setValue(settings.value("Interface/PopupSize", QSize(265, 150)).toSize().height());
    ui->widthSlider->setValue(settings.value("Interface/PopupSize", QSize(265, 150)).toSize().width());
    ui->fontSizeSlider->setValue(settings.value("Interface/FontSize", 10).toInt());
    ui->trayIconBox->setCurrentIndex(ui->trayIconBox->findData(settings.value("TrayIcon", ":/data/qweKadabra-tray.svg").toString()));
    ui->customIconLineEdit->setText(settings.value("CustomIconPath", "").toString());
    //+Add tray icon //+Добавить по иконке

    //Shortcuts //Горячие клавиши
    ui->convertGlobalKeySequence->setKeySequence(settings.value("Shortcuts/ConvertGlobalKeySequence", "Ctrl+Shift+S").toString());
    ui->convertAndReplaceKeySequence->setKeySequence(settings.value("Shortcuts/ConvertAndReplaceKeySequence", "Ctrl+Shift+D").toString());
    ui->copyKeySequence->setKeySequence(settings.value("Shortcuts/CopyKeySequence", "Ctrl+Shift+C").toString());
    ui->settingsKeySequence->setKeySequence(settings.value("Shortcuts/SettingsKeySequence", "Ctrl+Shift+A").toString());
    ui->convertMWKeySequence->setKeySequence(settings.value("Shortcuts/ConvertMWKeySequence", "Ctrl+Enter").toString());
    ui->exitKeySequence->setKeySequence(settings.value("Shortcuts/ExitKeySequence", "Ctrl+Q").toString());

}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_buttonBox_accepted()
{
    QSettings settings;
    if (settings.value("LocaleLanguage", "auto").toString() != ui->localeLanguage->currentData()) {
        settings.setValue("LocaleLanguage", ui->localeLanguage->currentData());
        localizationIsChanged = true;
    }

    //General //Главное
    settings.setValue("FirstPreferredLanguage", ui->firstPreferredLanguage->currentData());
    settings.setValue("SecondPreferredLanguage", ui->secondPreferredLanguage->currentData());
    settings.setValue("AutostartCheckBox", ui->autostartCheckBox->isChecked());
    settings.setValue("MinimizedCheckBox", ui->minimizedCheckBox->isChecked());
    settings.setValue("TrayCheckBox", ui->trayCheckBox->isChecked());

    //Interface //Интерфейс
    settings.setValue("Interface/Transparency",  static_cast<double>(ui->transparencySlider->value()) / 100);
    settings.setValue("Interface/PopupSize", QSize(ui->widthSlider->value(), ui->heightSlider->value()));
    settings.setValue("Interface/FontSize",  ui->fontSizeSlider->value());
    settings.setValue("TrayIcon", ui->trayIconBox->currentData());
    settings.setValue("CustomIconPath", ui->customIconLineEdit->text());
    //+Add tray icon //+Добавить по иконке

    //Shortcuts //Горячие клавиши
    settings.setValue("Shortcuts/ConvertGlobalKeySequence",  ui->convertGlobalKeySequence->keySequence());
    settings.setValue("Shortcuts/ConvertAndReplaceKeySequence",  ui->convertAndReplaceKeySequence->keySequence());
    settings.setValue("Shortcuts/CopyKeySequence",  ui->copyKeySequence->keySequence());
    settings.setValue("Shortcuts/SettingsKeySequence",  ui->settingsKeySequence->keySequence());
    settings.setValue("Shortcuts/ConvertMWKeySequence",  ui->convertMWKeySequence->keySequence());
    settings.setValue("Shortcuts/ExitKeySequence",  ui->exitKeySequence->keySequence());



    // Check if autostart options changed // Изменение настроек автостарта
#if defined(Q_OS_LINUX)
    QFile autorunFile(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/autostart/qweKadabra.desktop");
    if(ui->autostartCheckBox->isChecked()) {
        // Create autorun file if checked
        if (!autorunFile.exists()) {
            if (autorunFile.open(QFile::WriteOnly)) {
                QString autorunContent("[Desktop Entry]\n"
                                       "Type=Application\n"
                                       "Exec=crow\n"
                                       "Hidden=false\n"
                                       "NoDisplay=false\n"
                                       "Icon=qweKadabra\n"
                                       "Name=qweKadabra\n"
                                       "Comment=A simple translator that allows to translate and say selected text using the Google Translate API and much more\n"
                                       "Comment[ru]=Простой переводчик, который позволяет переводить и озвучивать выделенный текст с помощью Google Translate API, а также многое другое\n");
                QTextStream outStream(&autorunFile);
                outStream << autorunContent;
                autorunFile.close();
            }
            else
                qDebug() << tr("Unable to create autorun file");
        }
    }
    // Remove autorun file if box unchecked
    else
        if(autorunFile.exists())
            autorunFile.remove();
#elif defined(Q_OS_WIN)
    QSettings autostartSettings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    if (ui->autostartCheckBox->isChecked())
        autostartSettings.setValue("qweKadabra", QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
    else
        autostartSettings.remove("qweKadabra");
#endif
}

void SettingsDialog::on_resetShortcutsButton_clicked()
{
    ui->convertGlobalKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+S"));
    ui->convertAndReplaceKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+D"));
    ui->copyKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+C"));
    ui->settingsKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+A"));
    ui->convertMWKeySequence->setKeySequence(QKeySequence("Ctrl+Enter"));
    ui->exitKeySequence->setKeySequence(QKeySequence("Ctrl+Q"));
}

void SettingsDialog::on_resetAllButton_clicked()
{
    //General //Главное
    ui->localeLanguage->setCurrentIndex(0);
    ui->firstPreferredLanguage->setCurrentIndex(0);
    ui->secondPreferredLanguage->setCurrentIndex(1);
    ui->autostartCheckBox->setChecked(false);
    ui->minimizedCheckBox->setChecked(false);
    ui->trayCheckBox->setChecked(true);

    //Interface //Интерфейс
    ui->transparencySlider->setValue(100);
    ui->heightSlider->setValue(150);
    ui->widthSlider->setValue(265);
    ui->fontSizeSlider->setValue(10);
    ui->customIconLineEdit->setText("");
    ui->trayIconBox->setCurrentIndex(0);

    //Shortcuts //Горячие клавиши
    ui->convertGlobalKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+S"));
    ui->convertAndReplaceKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+D"));
    ui->copyKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+C"));
    ui->settingsKeySequence->setKeySequence(QKeySequence("Ctrl+Shift+A"));
    ui->convertMWKeySequence->setKeySequence(QKeySequence("Ctrl+Enter"));
    ui->exitKeySequence->setKeySequence(QKeySequence("Ctrl+Q"));
}

bool SettingsDialog::localizationChanged()
{
    return localizationIsChanged;
}

void SettingsDialog::on_customIconToolButton_clicked()
{
    QString path = ui->customIconLineEdit->text().left(ui->customIconLineEdit->text().lastIndexOf("/"));
    QString file = QFileDialog::getOpenFileName(this, tr("Select icon"), path, tr("Images (*.png *.ico *.svg *.jpg);;All files()"));
    if (file != "")
        ui->customIconLineEdit->setText(file);
}

void SettingsDialog::on_trayIconBox_currentIndexChanged(int index)
{
    if (index == 10) {
        ui->customLabel->setEnabled(true);
        ui->customIconLineEdit->setEnabled(true);
        ui->customIconToolButton->setEnabled(true);
    }
    else {
        ui->customLabel->setEnabled(false);
        ui->customIconLineEdit->setEnabled(false);
        ui->customIconToolButton->setEnabled(false);
    }
}
