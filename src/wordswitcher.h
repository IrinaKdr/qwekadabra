/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef WORDSWITCHER_H
#define WORDSWITCHER_H

#include <QString>
#include <QObject>

// Layouts
#define ENGLISH_LAYOUT \
    "`~!@#$%^&*()_+" \
    "qwertyuiop[]" \
    "QWERTYUIOP{}" \
    "asdfghjkl;'\\" \
    "ASDFGHJKL:\"|" \
    "zxcvbnm,./" \
    "ZXCVBNM<>?"
#define RUSSIAN_LAYOUT \
    "ёЁ!\"№;%:?*()_+" \
    "йцукенгшщзхъ" \
    "ЙЦУКЕНГШЩЗХЪ" \
    "фывапролджэ\\" \
    "ФЫВАПРОЛДЖЭ/" \
    "ячсмитьбю." \
    "ЯЧСМИТЬБЮ,"
#define UKRAINIAN_LAYOUT \
    "'ʼ!\"№;%:?*()_+" \
    "йцукенгшщзхї" \
    "ЙЦУКЕНГШЩЗХЇ" \
    "фівапролджєґ" \
    "ФІВАПРОЛДЖЄҐ" \
    "ячсмитьбю." \
    "ЯЧСМИТЬБЮ,"

class WordSwitcher : public QObject
{
    Q_OBJECT

public:
    enum Layout {
        Unknown = 0,
        English = 1033,
        Russian = 1049,
        Ukrainian = 1058
    };

    void switchLayout(const QString &data, Layout layoutTo, Layout layoutFrom = Unknown);
    QString getReplacedText() const;
    int getLayoutName();

private:
    QString getLayoutString(Layout layout);
    Layout getCurrentLayout();

    QString replacedText;
};

#endif // WORDSWITCHER_H
