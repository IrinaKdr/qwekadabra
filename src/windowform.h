/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef WINDOWFORM_H
#define WINDOWFORM_H

#include <QWidget>
#include <QString>
#include "wordswitcher.h"
#include "QShortcut"

namespace Ui {
class windowForm;
}

class windowForm : public QWidget
{
    Q_OBJECT

public:
    explicit windowForm(int layout, QWidget *parent = nullptr);
    void setData(QString data);
    void setSelectedText(QString text);
    ~windowForm();

private slots:
    void on_copyButton_clicked();
    void on_swichButton_clicked();
    void on_inputComboBox_currentIndexChanged(int index);
    void on_outputComboBox_currentIndexChanged(int index);

private:
    Ui::windowForm *ui;
    QString selectedText;
    WordSwitcher switcher;
    QShortcut *copyConvertedHotkey;

    int inputPreviousID;
    int outputPreviousID;
};

#endif // WINDOWFORM_H
