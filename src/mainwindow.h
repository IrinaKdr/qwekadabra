/*
 *  Copyright © 2018 Irina Kondratenko <irina.kdr@gmail.com>
 *
 *  This file is part of qweKadabra.
 *
 *  qweKadabra is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a get of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QSystemTrayIcon>
#include <QShortcut>
#include <QTranslator>


#include "wordswitcher.h"
#include "qhotkey.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void loadSettings();

    static int firstPreferedLanguage;
    static int secondPreferedLanguage;

    ~MainWindow();


private slots:
    void on_inputComboBox_currentIndexChanged(int index);
    void on_outputComboBox_currentIndexChanged(int index);
    void on_swapLayoutButton_clicked();
    void on_automaticReverseBox_toggled(bool checked);
    void on_copyFromButton_clicked();
    void on_copyToButton_clicked();
    void on_settingsButton_clicked();
    void on_swichButton_clicked();

    void showPopup();
    void switchSelection();
#if defined(Q_OS_WIN)
    void waitForKeyUnpressed(QHotkey *hotkey);
#endif

private:
    QString copySelectedText();
    void saveClipboard();
    void restoreClipboard();
#if defined(Q_OS_WIN)
    void pasteText(const QString &text);
#endif

    WordSwitcher switcher;

    QVariant originalClipboard;

    Ui::MainWindow *ui;
    QMenu *trayMenu;
    QSystemTrayIcon * trayIcon;
    QHotkey *showPopupHotkey;
    QHotkey *switchSelectionHotkey;
    QHotkey *settingsHotkey;
    QShortcut *exitShortcut;
    QShortcut *convertShortcut;
    QShortcut *copyConvertedHotkey;

    int inputPreviousID = 0;
    int outputPreviousID = 1;
    QTranslator translator;

};

#endif // MAINWINDOW_H
