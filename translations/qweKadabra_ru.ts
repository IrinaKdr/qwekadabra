<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <source>Copy source text to the clipboard</source>
        <translation>Копировать исходный текст в буфер обмена</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="47"/>
        <source>Automatic reverse layout</source>
        <translation>Автоматическая конвертация</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="75"/>
        <source>Expected layout</source>
        <translation>Раскладка в которую конвертировать</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="79"/>
        <location filename="../mainwindow.ui" line="120"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="97"/>
        <location filename="../mainwindow.ui" line="138"/>
        <source>Ukrainian</source>
        <translation>Украинский</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="116"/>
        <source>Source text layout</source>
        <translation>Раскладка исходного текста</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="153"/>
        <source>to layout</source>
        <translation>раскладка, в которую нужно конвертировать</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <source>from layout</source>
        <translation>раскладка, с которой нужно конвертировать</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Replace text</source>
        <translation>Конвертировать текст</translation>
    </message>
    <message>
        <source>Teplace text</source>
        <translation type="vanished">Конвертировать текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="187"/>
        <source>Swap layouts</source>
        <translation>Поменять местами раскладки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="216"/>
        <source>Copy result to the clipboard</source>
        <translation>Скопировать результат в буфер обмена</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="230"/>
        <source>Open settings</source>
        <translation>Открыть настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="62"/>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Show window</source>
        <translation>Показать окно</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="63"/>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="335"/>
        <location filename="../mainwindow.cpp" line="345"/>
        <source>Text field is empty</source>
        <translation>Текстовое поле пустое</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="53"/>
        <location filename="../settingsdialog.ui" line="103"/>
        <source>General</source>
        <translation>Основное</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="62"/>
        <source>Interface</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="71"/>
        <source>Shortcuts</source>
        <translation>Горячие клавиши</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="80"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="111"/>
        <source>Interface language</source>
        <translation>Язык интерфейса</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="119"/>
        <source>&lt;Auto&gt;</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="124"/>
        <location filename="../settingsdialog.ui" line="187"/>
        <location filename="../settingsdialog.ui" line="229"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="133"/>
        <location filename="../settingsdialog.ui" line="196"/>
        <location filename="../settingsdialog.ui" line="238"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="147"/>
        <source>Launch at startup</source>
        <translation>Запускать при старте системы</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="154"/>
        <source>Start minimized</source>
        <translation>Запускать свёрнутым</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="161"/>
        <source>Show icon in tray</source>
        <translation>Показать иконку в трее</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="171"/>
        <source>Сonverting</source>
        <translation>Конвертация</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="179"/>
        <source>First preferred language</source>
        <translation>Первый предпочитаемый язык</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="205"/>
        <location filename="../settingsdialog.ui" line="247"/>
        <source>Ukrainian</source>
        <translation>Украинский</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="221"/>
        <source>Second preferred language</source>
        <translation>Второй предпочитаемый язык</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="281"/>
        <source>Pop-up window</source>
        <translation>Всплывающее окно</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="326"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="365"/>
        <source>Height:</source>
        <translation>Высота:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="372"/>
        <source>Width:</source>
        <translation>Ширина:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="392"/>
        <source>Transparency:</source>
        <translation>Прозрачность:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="399"/>
        <source>Font size:</source>
        <translation>Размер шрифта:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="422"/>
        <source>Tray icon</source>
        <translation>Иконка в трее</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="428"/>
        <source>Icon:</source>
        <translation>Иконка:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="442"/>
        <source>Black</source>
        <translation>Черная</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="451"/>
        <source>White</source>
        <translation>Белая</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="460"/>
        <source>Color</source>
        <translation>Цветная</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="469"/>
        <source>Blue</source>
        <translation>Голубая</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="478"/>
        <source>Dark blue</source>
        <translation>Синяя</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="487"/>
        <source>Green</source>
        <translation>Зеленая</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="496"/>
        <source>Orange</source>
        <translation>Оранжевая</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="505"/>
        <source>Pink</source>
        <translation>Розовая</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="514"/>
        <source>Red</source>
        <translation>Красная</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="523"/>
        <source>Yellow</source>
        <translation>Жёлтая</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="532"/>
        <source>Custom</source>
        <translation>Установить вручную</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="543"/>
        <source>Custom:</source>
        <translation>Выбрать:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="562"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="591"/>
        <source>Global shortcuts</source>
        <translation>Глобальные горячие клавиши</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="600"/>
        <source>Copy converted text:</source>
        <translation>Копировать текст:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="607"/>
        <source>Convert selected from current layout:</source>
        <translation>Конвертировать выделенное:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="617"/>
        <source>Settings:</source>
        <translation>Открыть настройки:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="630"/>
        <source>Convert and replace selected text :</source>
        <translation>Конвертировать и заменить выделенное:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="640"/>
        <source>Main window shortcuts</source>
        <translation>Горячие клавиши главного окна</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="646"/>
        <source>Exit:</source>
        <translation>Выход:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="656"/>
        <source>Convert:</source>
        <translation>Конвертировать:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="700"/>
        <source>Reset all</source>
        <translation>Сбросить настройки</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="738"/>
        <source>qweKadabra</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="744"/>
        <source>License:</source>
        <translation>Лицензия:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="751"/>
        <source>Irina Kondratenko</source>
        <translation>Ирина Кондратенко</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="758"/>
        <source>E-mail:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="765"/>
        <source>GPL v3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="772"/>
        <source>Flag icons</source>
        <translation>Иконки флагов</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="779"/>
        <source>Autor:</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="793"/>
        <source>Version:</source>
        <translation>Версия:</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="800"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/madebybowtie/FlagKit&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;FlagKit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="807"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;mailto:sledgehammer999@qbittorrent.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;irina.kdr@gmail.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="855"/>
        <source>Reset all settings</source>
        <translation>Настройки по умолчанию</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="147"/>
        <source>Unable to create autorun file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="208"/>
        <source>Select icon</source>
        <translation>Выбрать Иконку</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="208"/>
        <source>Images (*.png *.ico *.svg *.jpg);;All files()</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WordSwitcher</name>
    <message>
        <location filename="../wordswitcher.cpp" line="21"/>
        <source>Unknown layout. You must select RU, ENG or UKR</source>
        <translation>Неизвестная раскладка. Вы должны выбрать доступную раскладку</translation>
    </message>
</context>
<context>
    <name>windowForm</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Всплывающее окно qweKadabra</translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="14"/>
        <source>qweKadabra</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="27"/>
        <location filename="../windowform.ui" line="69"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="36"/>
        <location filename="../windowform.ui" line="78"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="45"/>
        <location filename="../windowform.ui" line="87"/>
        <source>Ukrainian</source>
        <translation>Украинский</translation>
    </message>
    <message>
        <location filename="../windowform.cpp" line="90"/>
        <source>Text field is empty</source>
        <translation>Текстовое поле пустое</translation>
    </message>
</context>
</TS>
