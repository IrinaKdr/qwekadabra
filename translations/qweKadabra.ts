<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <source>Copy source text to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <source>Automatic reverse layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <source>Expected layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="78"/>
        <location filename="../mainwindow.ui" line="119"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="87"/>
        <location filename="../mainwindow.ui" line="128"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <location filename="../mainwindow.ui" line="137"/>
        <source>Ukrainian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="115"/>
        <source>Source text layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="152"/>
        <source>to layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="159"/>
        <source>from layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Teplace text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <source>Swap layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="213"/>
        <source>Copy result to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <source>Open settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="53"/>
        <source>Show window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="54"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="326"/>
        <location filename="../mainwindow.cpp" line="336"/>
        <source>Text field is empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="49"/>
        <location filename="../settingsdialog.ui" line="99"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="58"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="67"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="76"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="107"/>
        <source>Interface language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="115"/>
        <location filename="../settingsdialog.ui" line="178"/>
        <location filename="../settingsdialog.ui" line="220"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="124"/>
        <location filename="../settingsdialog.ui" line="187"/>
        <location filename="../settingsdialog.ui" line="229"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="138"/>
        <source>Launch at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="145"/>
        <source>Start minimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="152"/>
        <source>Show icon in tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="162"/>
        <source>Сonverting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="170"/>
        <source>First preferred language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="196"/>
        <location filename="../settingsdialog.ui" line="238"/>
        <source>Ukrainian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="212"/>
        <source>Second preferred language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="272"/>
        <source>Pop-up window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="317"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="356"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="363"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="383"/>
        <source>Transparency:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="390"/>
        <source>Font size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="413"/>
        <source>Tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="419"/>
        <source>Icon:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="436"/>
        <source>Custom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="448"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="477"/>
        <source>Global shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="486"/>
        <source>Copy converted text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="493"/>
        <source>Convert selected from current layout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="503"/>
        <source>Settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="516"/>
        <source>Convert and replace selected text :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="526"/>
        <source>Main window shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="532"/>
        <source>Exit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="542"/>
        <source>Convert:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="586"/>
        <source>Reset all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="621"/>
        <source>qweKadabra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="627"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="634"/>
        <source>Irina Kondratenko</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="641"/>
        <source>E-mail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="648"/>
        <source>GPL v3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="655"/>
        <source>Flag icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="662"/>
        <source>Autor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="676"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="683"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/madebybowtie/FlagKit&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;FlagKit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="690"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;mailto:sledgehammer999@qbittorrent.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;irina.kdr@gmail.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui" line="751"/>
        <source>Reset all settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="117"/>
        <source>Unable to create autorun file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WordSwitcher</name>
    <message>
        <location filename="../wordswitcher.cpp" line="21"/>
        <source>Unknown layout. You must select RU, ENG or UKR</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>windowForm</name>
    <message>
        <location filename="../windowform.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="23"/>
        <location filename="../windowform.ui" line="65"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="32"/>
        <location filename="../windowform.ui" line="74"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windowform.ui" line="41"/>
        <location filename="../windowform.ui" line="83"/>
        <source>Ukrainian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../windowform.cpp" line="90"/>
        <source>Text field is empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
