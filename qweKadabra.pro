#-------------------------------------------------
#
# Project created by Irina Kondratenko 2018-07-05T23:07:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14
TARGET = qweKadabra
TEMPLATE = app
VERSION = 0.8

# Windows specific stuff
QMAKE_TARGET_COMPANY = Irina Kondratenko
QMAKE_TARGET_DESCRIPTION = qweKadabra
QMAKE_TARGET_COPYRIGHT = Copyright © 2018 Irina Kondratenko
QMAKE_TARGET_PRODUCT = qweKadabra
RC_ICONS = dist\windows\icon.ico


include(src/third-party/qhotkey/qhotkey.pri)
include(src/third-party/singleapplication/singleapplication.pri)

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
        src/main.cpp \
        src/mainwindow.cpp \
        src/windowform.cpp \
        src/wordswitcher.cpp \
        src/settingsdialog.cpp

HEADERS += \
        src/mainwindow.h \
        src/windowform.h \
        src/wordswitcher.h \
        src/settingsdialog.h

FORMS += \
      src/mainwindow.ui \
      src/windowform.ui \
      src/settingsdialog.ui

RESOURCES += \
          resources.qrc \
          data/windows-icons.qrc

TRANSLATIONS += $$files(translations/qweKadabra_*.ts)

unix {
SOURCES += \
        src/third-party/XKeyboard.cpp

HEADERS += \
        src/third-party/XKeyboard.h \
        src/third-party/X11Exception.h
}
